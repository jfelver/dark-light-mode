import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnChanges, OnInit } from '@angular/core';
import { ColorSchemeService } from '../../services/color-scheme.service'

@Component({
  selector: 'app-theme-switcher',
  templateUrl: './theme-switcher.component.html',
  styleUrls: ['./theme-switcher.component.scss']
})
export class ThemeSwitcherComponent implements OnInit {
  public isDark: boolean;
  public themeMode: string = 'dark';
  public currentTheme: string;

  constructor(public colorSchemeService: ColorSchemeService) { }

  ngOnInit(): void {
    this.currentTheme = this.colorSchemeService._getColorScheme()
    this.currentTheme === 'light' ? this.isDark = true : this.isDark = false;
  }

  themeToggle() {
    this.currentTheme = this.colorSchemeService._getColorScheme()

    if (this.currentTheme === 'dark') {
      this.themeMode = 'light';
      this.setTheme(this.themeMode);
    }else{
      this.themeMode = 'dark';
      this.setTheme(this.themeMode);
    }
  }

  setTheme(theme: string) {
    this.colorSchemeService.update(theme);
  }


}
